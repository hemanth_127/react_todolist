import { useState } from 'react'

export function NewTodoForm ({ onSubmit }) {
  const [newItem, setNewItem] = useState('')

  // Add item to todos array
  function handleSubmit (e) {
    e.preventDefault()
    console.log(onSubmit)
    onSubmit(newItem)

    setNewItem('')
  }

  //if you want to see in console
  // function handleChange(e) {
  //   console.log(e.target.value); // Log the value of e.target
  //   setNewItem(e.target.value);
  // }

  return (
    <form className='new-item-form' onSubmit={handleSubmit}>
      <div className='form-row'>
        <label htmlFor='item'>New Item</label>
        <input
          type='text'
          id='item'
          value={newItem}
          onChange={e => setNewItem(e.target.value)}
          
          // if you want to see in console
          // onChange={handleChange}
        />
      </div>
      <button className='btn'>ADD</button>
    </form>
  )
}
