import { TodoItem } from './TodoItem'
export function TodoList ({ todos, toggleTodo, deletetodo }) {
  return (
    <ul className='list'>
      {todos.length === 0 && 'Oops! No Task is added'}
      {todos.map(todo => {
        return (
          <TodoItem
            {...todo}
            key={todo.id}
            toggleTodo={toggleTodo}
            deletetodo={deletetodo}
          />
        )
      })}
    </ul>
  )
}
