import { useState } from 'react'
import './style.css'
import { NewTodoForm } from './NewTodoForm'
import { TodoList } from './TodoList'
import { useEffect } from 'react'

export default function App () {
  const [todos, setTodos] = useState([])

  // const [todos, setTodos] = useState(() => {
  //   return JSON.parse(localStorage.getItem('ITEMS')) || []
  // })

  // //adding to local storage
  // useEffect(() => {
  //   localStorage.setItem('ITEMS', JSON.stringify(todos))
  // }, [todos])

  // Submit a new todo item to the list
  function addTodo (title) {
    setTodos(currentTodos => {
      return [
        ...currentTodos,
        { id: crypto.randomUUID(), title, completed: false }
      ]
    })
  }

  // Check off an item
  function toggleTodo (id, completed) {
    setTodos(currentTodos => {
      return currentTodos.map(todo => {
        if (todo.id === id) {
          return { ...todo, completed }
        }
        return todo
      })
    })
  }

  // delete
  function deletetodo (id) {
    setTodos(currentTodos => {
      return currentTodos.filter(todo => todo.id !== id)
    })
    console.log(todos)
  }

  return (
    <>
      <NewTodoForm onSubmit={addTodo} />
      <h1 className='header'>Todo List</h1>
      <TodoList todos={todos} toggleTodo={toggleTodo} deletetodo={deletetodo} />
    </>
  )
}
